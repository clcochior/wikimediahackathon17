===This project was developed at the Wikimedia Hackathon 2017 by Strainu and Clco===

In this repository you will find a Telegram bot and a bot set to run on the Romanian Wikipedia. Their function is to find articles with only one contributor and list them to interested users. The bot's intention is to encourage multiple points of view, and also to surface niche articles.

