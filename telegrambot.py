# -*- coding: utf-8 -*-
from telegram.ext import Updater
import pywikibot, catlib
from pywikibot import pagegenerators

updater = Updater(token='389716967:AAGtUdbTj6MOMUml3V12gcumQX26Bov9OFk')

dispatcher = updater.dispatcher

import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

def start(bot, update):
	bot.send_message(chat_id=update.message.chat_id, text="Hi, I'm wikisolo. Write '/wiki' and I will send you articles who have only been edited by one user. The more editors, the merrier.")

from telegram.ext import CommandHandler
start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)


# def echo(bot, update):
# 	bot.send_message(chat_id=update.message.chat_id, text=update.message.text)

# from telegram.ext import MessageHandler, Filters
# echo_handler = MessageHandler(Filters.text, echo)
# dispatcher.add_handler(echo_handler)


def singlepage(bot, update):
	import pywikibot
	from pywikibot import pagegenerators
	import random
	site = pywikibot.Site()
	cat = pywikibot.Category(site, u'Categorie:Articole_biografice_ale_personalităților_în_viață')
	gen = pagegenerators.CategorizedPageGenerator(cat)

	count=random.randint(1,10)
	
	for page in gen:
		page=page.toggleTalkPage()
		if not page.exists():
			continue
		number_of_contributors=page.contributors()
		print number_of_contributors
		if len(number_of_contributors)>=1:
			continue
		count=count-1
		if count==0:
			bot.send_message(chat_id=update.message.chat_id, text="The more editors the merrier. Here is a page with only one custodian: https://ro.wikipedia.org/wiki/"+page.title(asUrl=True))
			return

wiki_handler = CommandHandler('wiki', singlepage)
dispatcher.add_handler(wiki_handler)



updater.start_polling()
