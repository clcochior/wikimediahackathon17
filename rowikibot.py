# -*- coding: utf-8 -*-
import pywikibot, catlib
#import unicode
from pywikibot import pagegenerators

site = pywikibot.Site()
cat = pywikibot.Category(site, u'Categorie:Articole_biografice_ale_personalităților_în_viață')
gen = pagegenerators.CategorizedPageGenerator(cat)

print cat.exists()

for page in gen:
	page=page.toggleTalkPage()
	if not page.exists():
		continue
	number_of_contributors=page.contributors()
	print number_of_contributors
	if len(number_of_contributors)>=2:
		continue
	# subscriberspage = pywikibot.Page(site, u"Utilizator:Andrebot/Abonați newsletter sysop")
	subscriberlinks=[]
  	# subscriberlinks = subscriberspage.linkedPages()
  	subscriberlinks.append(pywikibot.Page(site, u"Utilizator:Strainubot"))

  	notifiedpage = pywikibot.Page(site, unicode("Utilizator:Strainubot/notifiedpages", "utf-8"))
  	notifiedlinks = notifiedpage.linkedPages()

	if page in notifiedlinks:
		continue

	for subscribername in subscriberlinks:
		  # text = page.get()
		subscribertalkpage = subscribername.toggleTalkPage()
		if subscribertalkpage.exists():
			text=subscribertalkpage.get()
		else: 
			text=u""
		text=text+u"""
== Biografie potențial orfană ==
Articolul [[%s]] despre o persoană în viață a fost editat până acum de un singur utilizator. Vă rugăm să verificați corectitudinea articolului. Nu veți mai fi notificat despre acest articol în viitor.--~~~~

""" %page.title()
		subscribertalkpage.put(text, minorEdit=False, comment = unicode("Robot: notificare biografii posibil orfane", "utf-8"))
		textexistent=notifiedpage.get()
		textexistent=textexistent+u"\n*[["+page.title()+"]]"
		notifiedpage.put(textexistent, minorEdit=False, comment = unicode("Robot: adăugare site deja menționat", "utf-8"))
		exit()
 